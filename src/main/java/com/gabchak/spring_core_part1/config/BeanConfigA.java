package com.gabchak.spring_core_part1.config;

import com.gabchak.spring_core_part1.beans.BeanB;
import com.gabchak.spring_core_part1.beans.BeanC;
import com.gabchak.spring_core_part1.beans.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class BeanConfigA {

    @DependsOn(value = "BeanD")
    @Bean(value = "BeanB", initMethod = "init", destroyMethod = "destroy")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @DependsOn(value = {"BeanD", "BeanB"})
    @Bean(value = "BeanC", initMethod = "init", destroyMethod = "destroy")
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(value = "BeanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD();
    }
}