package com.gabchak.spring_core_part1.config;

import com.gabchak.spring_core_part1.beans.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(BeanConfigA.class)
public class BeanConfigB {

    @Bean("beanA_BC")
    public BeanA getBeanA_BC(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB.getName(), beanC.getValue());
    }

    @Bean("beanA_BD")
    public BeanA getBeanA_BD(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB.getName(), beanD.getValue());
    }

    @Bean("beanA_CD")
    public BeanA getBeanA_CD(BeanC beanC, BeanD beanD) {
        return new BeanA(beanC.getName(), beanD.getValue());
    }

    @Bean("beanE_A_BC")
    public BeanE getBeanE_A_BC(BeanA beanA_BC) {
        return new BeanE(beanA_BC.getName(), beanA_BC.getValue());
    }

    @Bean("beanE_A_BD")
    public BeanE getBeanE_A_BD(BeanA beanA_BD) {
        return new BeanE(beanA_BD.getName(), beanA_BD.getValue());
    }

    @Bean("beanE_A_CD")
    public BeanE getBeanE_A_CD(BeanA beanA_CD) {
        return new BeanE(beanA_CD.getName(), beanA_CD.getValue());
    }

    @Lazy
    @Bean("beanF")
    public BeanF getBeanF(BeanB beanB) {
        return new BeanF(beanB.getName(), beanB.getValue());
    }

    @Bean
    CustomBeanFactoryPostProcessor CustomBeanFactoryPostProcessor(){
        return new CustomBeanFactoryPostProcessor();
    }

    @Bean
    CustomBeanPostProcessor myConfigBean () {
        return new CustomBeanPostProcessor();
    }

}
