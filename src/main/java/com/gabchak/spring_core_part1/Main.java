package com.gabchak.spring_core_part1;

import com.gabchak.spring_core_part1.config.BeanConfigB;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigB.class);
        Arrays.stream(context.getBeanDefinitionNames())
                .forEach(beanName -> System.out.println(context.getBeanDefinition(beanName)));
        context.close();

    }
}
