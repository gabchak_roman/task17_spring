package com.gabchak.spring_core_part1.beans;

import com.gabchak.spring_core_part1.beans.interfaces.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CustomBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator) {
            if (((BeanValidator) bean).validate()){
                System.out.println(beanName + " is valid.");
            }
            else {
                System.out.println(beanName + " is not valid!");
            }
        }
        return bean;
    }
}

