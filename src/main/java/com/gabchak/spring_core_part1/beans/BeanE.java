package com.gabchak.spring_core_part1.beans;

import com.gabchak.spring_core_part1.beans.interfaces.BeanValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.Serializable;

public class BeanE implements Serializable, BeanValidator {

    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" + name + " : " + value + '}';
    }

    @Override
    public boolean validate() {
        return (name != null && !name.isEmpty()) && value > 0;
    }

    @PostConstruct
    public void postConstruct() {
        // действия после создания объекта
        System.out.println(this.getClass().getSimpleName() + ": postConstruct");
    }

    @PreDestroy
    public void preDestroy() {
        // действия перед уничтожением объекта
        System.out.println(this.getClass().getSimpleName() + ": preDestroy");
    }
}