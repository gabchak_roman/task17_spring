package com.gabchak.spring_core_part1.beans;

import com.gabchak.spring_core_part1.beans.interfaces.BeanValidator;

import java.io.Serializable;

public class BeanF implements Serializable, BeanValidator {

    private String name;
    private int value;

    public BeanF() {
    }

    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" + name + " : " + value + '}';
    }

    @Override
    public boolean validate() {
        return (name != null && !name.isEmpty()) && value > 0;
    }
}