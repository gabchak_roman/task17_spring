package com.gabchak.spring_core_part1.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class CustomBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    private void init(){
        System.out.println(this.getClass().getSimpleName() + " init()");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory)
            throws BeansException {
        BeanDefinition beanB = configurableListableBeanFactory.getBeanDefinition("BeanB");
        System.out.println("Previous init method of BeanB: " + beanB.getInitMethodName());
        System.out.println("new init method will be initB2");
        beanB.setInitMethodName("initB2");
    }

}
