package com.gabchak.spring_core_part1.beans;

import com.gabchak.spring_core_part1.beans.interfaces.BeanValidator;
import com.gabchak.spring_core_part1.beans.interfaces.DestroyMethod;
import com.gabchak.spring_core_part1.beans.interfaces.InitMethod;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

public class BeanD implements Serializable, BeanValidator, DestroyMethod, InitMethod {

    @Value("${beanD.name}")
    private String name;

    @Value("${beanD.value}")
    private int value;

    public BeanD() {
    }

    public BeanD(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" + name + " : " + value + '}';
    }

    @Override
    public boolean validate() {
        return (name != null && !name.isEmpty()) && value > 0;
    }

    @Override
    public void init() {
        System.out.println(this.getClass().getSimpleName() + ": initMethod");
    }

    @Override
    public void destroy() {
        System.out.println(this.getClass().getSimpleName() + ": destroyMethod");
    }
}