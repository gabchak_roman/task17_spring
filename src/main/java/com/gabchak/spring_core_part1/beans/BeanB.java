package com.gabchak.spring_core_part1.beans;

import com.gabchak.spring_core_part1.beans.interfaces.BeanValidator;
import com.gabchak.spring_core_part1.beans.interfaces.DestroyMethod;
import com.gabchak.spring_core_part1.beans.interfaces.InitMethod;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import java.io.Serializable;

public class BeanB implements Serializable, BeanValidator, DestroyMethod, InitMethod {

    @Value("${beanB.name}")
    private String name;

    @Value("${beanB.value}")
    private int value;

    public BeanB() {
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" + name + " : " + value + '}';
    }

    @Override
    public boolean validate() {
        return (name != null && !name.isEmpty()) && value > 0;
    }

    @Override
    public void init() {
        System.out.println(this.getClass().getSimpleName() + ": initMethod");
    }


    public void initB2() {
        System.out.println(this.getClass().getSimpleName() + ": initB2Method");
    }

    @Override
    public void destroy() {
        System.out.println(this.getClass().getSimpleName() + ": destroyMethod");
    }
}