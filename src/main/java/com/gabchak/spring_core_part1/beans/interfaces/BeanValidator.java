package com.gabchak.spring_core_part1.beans.interfaces;

public interface BeanValidator {

    boolean validate();
}
