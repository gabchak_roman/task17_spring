package com.gabchak.spring_core_part1.beans.interfaces;

public interface DestroyMethod {
    void destroy ();
}
