package com.gabchak.spring_core_part1.beans.interfaces;

public interface InitMethod {
    void init();
}
