package com.gabchak.spring_core_part1.beans;

import com.gabchak.spring_core_part1.beans.interfaces.BeanValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.io.Serializable;


public class BeanA implements Serializable, BeanValidator, InitializingBean, DisposableBean {

    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" + name + " : " + value + '}';
    }

    @Override
    public boolean validate() {
        return (name != null && !name.isEmpty()) && value > 0;
    }

    @Override
    public void afterPropertiesSet() {
        System.out.println(this.getClass().getSimpleName() + ": afterPropertiesSet");
    }

    @Override
    public void destroy() {
        System.out.println(this.getClass().getSimpleName() + ": destroy");
    }
}
