package com.gabchak.spring_core_part2.config;

import org.springframework.context.annotation.Import;

@Import({Configuration1.class, Configuration2.class, Configuration3.class})
public class MainConfiguration {
}
