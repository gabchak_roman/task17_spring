package com.gabchak.spring_core_part2.config;

import com.gabchak.spring_core_part2.beans.beans3.BeanD;
import com.gabchak.spring_core_part2.beans.beans3.BeanF;
import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanC;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(value = "com.gabchak.spring_core_part2.beans.beans2")
@ComponentScan(basePackages = "com.gabchak.spring_core_part2.beans.beans3", useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = {BeanD.class, BeanF.class}))
@Profile("ProfileB")
public class Configuration2 {

    @Bean
    public OtherBeanC otherBeanC (){
        return new OtherBeanC();
    }

}
