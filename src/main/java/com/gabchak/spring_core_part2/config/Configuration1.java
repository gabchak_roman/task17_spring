package com.gabchak.spring_core_part2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan(value = "com.gabchak.spring_core_part2.beans.beans1")
@Profile("ProfileA")
public class Configuration1 {
}
