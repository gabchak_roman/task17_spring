package com.gabchak.spring_core_part2.beans.beans1;

import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanA;
import org.springframework.stereotype.Component;

@Component

public class BeanA {

    OtherBeanA otherBeanA;

}
