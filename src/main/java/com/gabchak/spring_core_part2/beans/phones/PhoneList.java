package com.gabchak.spring_core_part2.beans.phones;

import com.gabchak.spring_core_part2.beans.phones.interfaces.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class PhoneList {

    private List<Phone> phoneList;

    @Autowired
    public PhoneList(List<Phone> phoneList) {
        this.phoneList = phoneList;
    }

    public void printPhones (){
        AtomicInteger num = new AtomicInteger();
        System.out.println("------ Phone list -----");
        phoneList.forEach(phone -> System.out.println(num.incrementAndGet() + ".\t" + phone.getPhone()));
        System.out.println("-----------------------");
    }

    @Override
    public String toString() {
        return "PhoneList{" +
                "phoneList=" + phoneList +
                '}';
    }
}
