package com.gabchak.spring_core_part2.beans.phones;

import com.gabchak.spring_core_part2.beans.phones.interfaces.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Phones2 {

    @Autowired
    private Phone phoneIphone;

    @Autowired
    @Qualifier("LG")
    private Phone phoneLG;

    @Autowired
    @Qualifier("Samsung")
    private Phone phoneSamsung;

    @Autowired
    @Qualifier("sony")
    private Phone phoneSony;

//    @PostConstruct
//    public void printPhones (){
//        System.out.println(phoneIphone.getPhone());
//        System.out.println(phoneLG.getPhone());
//        System.out.println(phoneSamsung.getPhone());
//        System.out.println(phoneSony.getPhone());
//    }

}
