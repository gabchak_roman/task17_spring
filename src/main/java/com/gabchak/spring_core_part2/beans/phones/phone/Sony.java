package com.gabchak.spring_core_part2.beans.phones.phone;

import com.gabchak.spring_core_part2.beans.phones.interfaces.Phone;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class Sony implements Phone {

    @Override
    public String getPhone() {
        return Sony.class.getSimpleName() + " Z2";
    }
}
