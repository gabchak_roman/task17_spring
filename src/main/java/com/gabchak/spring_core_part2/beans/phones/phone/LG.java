package com.gabchak.spring_core_part2.beans.phones.phone;

import com.gabchak.spring_core_part2.beans.phones.interfaces.Phone;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order
public class LG implements Phone {

    @Override
    public String getPhone() {
        return LG.class.getSimpleName() + " k5";
    }
}
