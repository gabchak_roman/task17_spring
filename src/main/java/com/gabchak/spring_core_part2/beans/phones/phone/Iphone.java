package com.gabchak.spring_core_part2.beans.phones.phone;

import com.gabchak.spring_core_part2.beans.phones.interfaces.Phone;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@Primary
public class Iphone implements Phone {

    @Override
    public String getPhone() {
        return Iphone.class.getSimpleName() + " 10";
    }
}
