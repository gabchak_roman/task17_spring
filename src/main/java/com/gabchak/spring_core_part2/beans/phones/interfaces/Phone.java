package com.gabchak.spring_core_part2.beans.phones.interfaces;

public interface Phone {
        String getPhone ();
}