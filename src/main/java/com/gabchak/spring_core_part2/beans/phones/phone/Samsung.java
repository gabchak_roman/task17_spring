package com.gabchak.spring_core_part2.beans.phones.phone;

import com.gabchak.spring_core_part2.beans.phones.interfaces.Phone;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("Samsung")
@Order(1)
public class Samsung implements Phone {

    @Override
    public String getPhone() {
        return Samsung.class.getSimpleName() + " S10";
    }
}
