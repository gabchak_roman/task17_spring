package com.gabchak.spring_core_part2.beans.otherBeans.autowierdBeans;

import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanA;
import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanB;
import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component

public class Constructor {

    private OtherBeanA otherBeanA;
    private OtherBeanB otherBeanB;
    private OtherBeanC otherBeanC;

    @Autowired
    public Constructor(OtherBeanA otherBeanA, OtherBeanB otherBeanB, @Qualifier("OBC") OtherBeanC otherBeanC) {
        this.otherBeanA = otherBeanA;
        this.otherBeanB = otherBeanB;
        this.otherBeanC = otherBeanC;
    }

    @Override
    public String toString() {
        return "Constructor{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                ", otherBeanC=" + otherBeanC +
                '}';
    }
}
