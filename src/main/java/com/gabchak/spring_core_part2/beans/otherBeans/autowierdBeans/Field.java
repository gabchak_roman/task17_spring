package com.gabchak.spring_core_part2.beans.otherBeans.autowierdBeans;

import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanA;
import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanB;
import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Field {

    @Autowired
    private OtherBeanA otherBeanA;

    @Autowired
    private OtherBeanB otherBeanB;

    @Autowired
    private OtherBeanC otherBeanC;

    @Override
    public String toString() {
        return "Field{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                ", otherBeanC=" + otherBeanC +
                '}';
    }
}
