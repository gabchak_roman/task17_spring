package com.gabchak.spring_core_part2.beans.otherBeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanB {
}
