package com.gabchak.spring_core_part2;

import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanA;
import com.gabchak.spring_core_part2.beans.otherBeans.OtherBeanB;
import com.gabchak.spring_core_part2.beans.phones.PhoneList;
import com.gabchak.spring_core_part2.config.MainConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        if (args.length == 1){
            System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, args[0]);
        }

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfiguration.class);

        Arrays.stream(context.getBeanDefinitionNames()).forEach(
                name -> System.out.println(context.getBeanDefinition(name).getBeanClassName())
        );

        context.getBean(PhoneList.class).printPhones();

        System.out.println("OtherBeanA @Scope(\"singleton\") hash: "
                + System.identityHashCode(context.getBean(OtherBeanA.class)));
        System.out.println("OtherBeanA @Scope(\"singleton\") hash: "
                + System.identityHashCode(context.getBean(OtherBeanA.class)));

        System.out.println("OtherBeanB @Scope(\"prototype\") hash: "
                + System.identityHashCode(context.getBean(OtherBeanB.class)));
        System.out.println("OtherBeanB @Scope(\"prototype\") hash: "
                + System.identityHashCode(context.getBean(OtherBeanB.class)));

    }
}
